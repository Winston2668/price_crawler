import csv
import datetime
import decimal
from django.http import HttpResponse
from django.contrib import admin
from django.template.response import TemplateResponse
from .models import Product, ProductPrice, SelectedProduct, SelectedProductPrice

class ExportCsvMixin:
    def export_formatted_price(self, request, queryset):
        meta = self.model._meta
        
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        
        if str(self.model._meta) == 'coles.selectedproduct':
            dates = SelectedProductPrice.objects.values('date').distinct()   
            for sp in SelectedProductPrice.objects.all():
                if sp.price is None:
                    sp.update_price()
        else:
            dates = ProductPrice.objects.values('date').distinct()
        dates = dates.order_by('date')
        rows = []
        first_row = ['Product ', 'Type ', 'Source '] + list(map(lambda date: date['date'].strftime('%Y-%m-%d'), dates))   
        writer.writerow(first_row)
        for product in queryset:
            data = []
            data.append(product.brand + ' ' + product.name)
            data.append(product.type)
            data.append(product.source)
            for d in dates:
                price_info = product.date_price.filter(date=d['date'])
                if price_info.count() > 0:
                    data.append(product.date_price.filter(date=d['date']).first().price)
                else:
                    data.append(' ')
            row = writer.writerow(data)
        return response
    export_formatted_price.short_description = "Export to excel"


class ExportTableMixin:
    def export_table_products(self, request, queryset):
        meta = self.model._meta
        
        for product in queryset:
            product_data = SelectedProduct()
            product_data.name = product.name
            product_data.brand = product.brand        
            product_data.type = product.type
            product_data.link = product.link
         
            sp, created = SelectedProduct.objects.get_or_create(
                name = product.name,
                brand = product.brand,
                type = product.type,
                link = product.link,
                source = product.source
            )
            sp.product = product
            sp.save()
 
            for product_price in product.date_price.all():
            #     product_info = SelectedProductPrice()
            #     product_info.price_str = product_price.price_str
            #     product_info.is_special = product_price.is_special
            # #    product_info.product = sp
            #     product_info.date = product_price.date
                p, created = SelectedProductPrice.objects.get_or_create(
                    price_str = product_price.price_str,
                    is_special = product_price.is_special,
                    product = sp,
                    date = product_price.date
                )
            
        return TemplateResponse(request, "coles/confirmation.html")

    export_table_products.short_description = "Export to new table"


class ProductPriceInline(admin.TabularInline):
    model = ProductPrice


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportCsvMixin, ExportTableMixin):
    list_filter = ('type',)
    search_fields = ['name']
    actions = ["export_formatted_price", "export_table_products"]
    inlines = [
        ProductPriceInline,
    ]


class SelectedProductPriceInline(admin.TabularInline):
    model = SelectedProductPrice


@admin.register(SelectedProduct)
class SelectedProductAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_filter = ('type',)
    search_fields = ['name']
    actions = ["export_formatted_price"]
    inlines = [
        SelectedProductPriceInline,
    ]