from django.db import models
from decimal import Decimal
import datetime
import re
# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=128, null=True)
    brand = models.CharField(max_length=128, null=True)
    type = models.CharField(max_length=128, null=True)
    link = models.TextField(default='')
    source = models.CharField(max_length=128, null=True)
    
    def __str__(self):
        if self.date_price:
            try:
                return self.brand + ' ' + self.name + ': ' + ',     '.join(list(map(lambda p: p.price_str, self.date_price.all())))
            except:
                return 'data failed'
        else:
            return self.brand + ' ' + self.name
      
            
class ProductPrice(models.Model):
    price_str  = models.CharField(max_length=128, null=True)
    price = models.DecimalField(null=True, max_digits=16, decimal_places=2)
    date = models.DateField(default=datetime.date.today, null=True) 
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="date_price",null=True)
    is_special = models.BooleanField(default=False)
            
    def save(self):
        #do some custom processing here: Example: convert Image resolution to a normalized value
        if self.price_str is not None and self.product.source == 'coles':
            price = self.price_str.split(' ')[0].replace('$', '')
            price = Decimal(price)
            self.price = price
        elif self.product.source == 'coles':
            self.price = -1
            
        if self.price_str is not None and self.product.source == 'woolworths':
            try:
                price = re.search("-?\d+(\.\d{1,2})?", self.price_str).group(0)
                price = Decimal(price)
                self.price = price
            except:
                self.price = -1
        elif self.product.source == 'woolworths':
            self.price = -1    
        super(ProductPrice, self).save()  
        
        
class SelectedProduct(models.Model):
    name = models.CharField(max_length=128, null=True)
    brand = models.CharField(max_length=128, null=True)
    type = models.CharField(max_length=128, null=True)
    link = models.TextField(default='')
    source = models.CharField(max_length=128, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="selected",null=True)
    
    def __str__(self):
        if self.date_price:
            try:
                return self.brand + ' ' + self.name + ': ' + ',     '.join(list(map(lambda p: p.price_str, self.date_price.all())))
            except:
                return 'data failed'
        else:
            return self.brand + ' ' + self.name
      
            
class SelectedProductPrice(models.Model):
    price_str  = models.CharField(max_length=128, null=True)
    price = models.DecimalField(null=True, max_digits=16, decimal_places=2)
    date = models.DateField(default=datetime.date.today, null=True) 
    product = models.ForeignKey(SelectedProduct, on_delete=models.CASCADE, related_name="date_price",null=True)
    is_special = models.BooleanField(default=False)
    
    def update_price(self):
        #do some custom processing here: Example: convert Image resolution to a normalized value
        if self.price_str is not None and self.product.source == 'coles':
            price = self.price_str.split(' ')[0].replace('$', '')
            price = Decimal(price)
            self.price = price
        elif self.product.source == 'coles':
            self.price = -1
            
        if self.price_str is not None and self.product.source == 'woolworths':
            try:
                price = re.search("-?\d+(\.\d{1,2})?", self.price_str).group(0)
                price = Decimal(price)
                self.price = price
            except:
                self.price = -1
        elif self.product.source == 'woolworths':
            self.price = -1    
        self.save()