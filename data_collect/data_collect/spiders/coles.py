# -*- coding: utf-8 -*-
import scrapy
import datetime
from data_collect.items import ProductItem

class ColesSpider(scrapy.Spider):
    name = 'coles'
    allowed_domains = ['shop.coles.com.au']
    start_urls = [
    #'https://shop.coles.com.au/a/a-national/everything/browse/fruit-vegetables/boxes-fruit-vege/fruit-vege-boxes?pageNumber=1',    
    'https://shop.coles.com.au/a/a-national/everything/browse/fruit-vegetables/fruit?pageNumber=1',
    'https://shop.coles.com.au/a/a-national/everything/browse/fruit-vegetables/vegetables?pageNumber=1',
    'https://shop.coles.com.au/a/a-national/everything/browse/fruit-vegetables/salad-herbs?pageNumber=1',
    'https://shop.coles.com.au/a/a-national/everything/browse/fruit-vegetables/nuts-dried-fruit?pageNumber=1',
    ]

    def parse(self, response):
        # Get the current page index. 
        page_number = len(response.xpath("//li[@class='page-number']"))
        page_number = 1 if page_number == 0 else page_number
        page_number = range(0, page_number)
        
        for page_index in page_number:
            next_page = response.url[:-1] + str(page_index + 1)
            yield scrapy.Request(next_page, callback=self.parse_page)
    
    def parse_page(self, response):
        # Extract data.
        products_infos = response.xpath("//div[@class='product']")
        print('url this' + response.url)
        for products_info in products_infos:
            product = ProductItem()
            product['name'] = products_info.xpath("descendant::span[@class='product-name']//text()").extract_first()
            product['brand'] = products_info.xpath("descendant::span[@class='product-brand']//text()").extract_first()
            product['product_link'] = 'https://shop.coles.com.au' + products_info.xpath("descendant::div[@class='product-image']/a/@href").extract_first()
            product['date'] = datetime.datetime.today().strftime('%Y-%m-%d')
            special_icon = products_info.xpath("descendant::svg[@class='icon icon-specials-badge']").extract_first()
            product['is_special'] = 1 if special_icon else 0
            product['type'] = response.url.split('?')[0].split('/')[-1]
            product['price'] = products_info.xpath("descendant::span[@class='package-price']//text()").extract_first()
            product['source'] = 'coles'
            yield product
            
        products_infos = response.xpath("//div[@class='product product-specials']")    
        for products_info in products_infos:
            product = ProductItem()
            product['name'] = products_info.xpath("descendant::span[@class='product-name']//text()").extract_first()
            product['brand'] = products_info.xpath("descendant::span[@class='product-brand']//text()").extract_first()
            product['product_link'] = 'https://shop.coles.com.au' + products_info.xpath("descendant::div[@class='product-image']/a/@href").extract_first()
            product['date'] = datetime.datetime.today().strftime('%Y-%m-%d')
            special_icon = products_info.xpath("descendant::svg[@class='icon icon-specials-badge']").extract_first()
            product['is_special'] = 1
            product['type'] = response.url.split('?')[0].split('/')[-1]
            product['price'] = products_info.xpath("descendant::span[@class='package-price']//text()").extract_first()
            product['source'] = 'coles'
            yield product

