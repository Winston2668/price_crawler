# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ProductItem(scrapy.Item):
    name = scrapy.Field()
    brand = scrapy.Field()
    type = scrapy.Field()
    price = scrapy.Field()
    date = scrapy.Field()
    product_link = scrapy.Field()
    is_special = scrapy.Field()
    source = scrapy.Field()
 