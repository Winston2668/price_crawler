import os
import sys
import django

# Setting up django env.
sys.path.insert(0, '/home/ubuntu/workspace/price_crawler/data_site')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "data_site.settings")
django.setup()

from coles.models import Product


