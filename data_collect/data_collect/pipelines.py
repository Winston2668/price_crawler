# -*- coding: utf-8 -*-
import datetime
import decimal
from coles.models import Product, ProductPrice, SelectedProduct, SelectedProductPrice

class DataCollectPipeline(object):
    def process_item(self, product, spider):
        product_data = Product()
        product_data.name = product['name']
        product_data.brand = product['brand']         
        product_data.type = product['type']
        product_data.link = product['product_link']
        
        p, created = Product.objects.get_or_create(
            name = product['name'],
            brand = product['brand'],
            type = product['type'],
            link = product['product_link'],
            source = product['source']
        )
        
        product_info = ProductPrice()
        product_info.price_str = product['price']
        product_info.is_special = True if (product['is_special']==1) else False
        product_info.product = p

        if p.date_price.filter(date=(datetime.datetime.today())).count() == 0:
            product_info.date = datetime.datetime.today()
            product_info.save()
        else:
            product_info = p.date_price.filter(date=(datetime.datetime.today())).first()
            
            
        if SelectedProduct.objects.filter(product = p).count() != 0:
            p = SelectedProduct.objects.get(product = p)
            if p.date_price.filter(date=(datetime.datetime.today())).count() == 0:
                selected_product_info = SelectedProductPrice()
                selected_product_info.price_str = product_info.price_str
                selected_product_info.is_special = product_info.is_special
                selected_product_info.product = p
                selected_product_info.date = product_info.date
                selected_product_info.price = product_info.price
                selected_product_info.save()
            
        return product
